#include <QTRSensors.h>
#include <NewPing.h>
#include "pinsSensors.h"

// us sensors
#define NUM_US 8
#define MAX_DISTANCE 300

float usFilteredValues[NUM_US];

NewPing usSensors[NUM_US] = {
    {US_FRONT_RIGHT_TRIG_PIN, US_FRONT_RIGHT_ECHO_PIN, MAX_DISTANCE},
    {US_RIGHT_FRONT_TRIG_PIN, US_RIGHT_FRONT_ECHO_PIN, MAX_DISTANCE},
    {US_RIGHT_TRIG_PIN, US_RIGHT_ECHO_PIN, MAX_DISTANCE},
    {US_BACK_RIGHT_TRIG_PIN, US_BACK_RIGHT_ECHO_PIN, MAX_DISTANCE},
    {US_BACK_LEFT_TRIG_PIN, US_BACK_LEFT_ECHO_PIN, MAX_DISTANCE},
    {US_LEFT_TRIG_PIN, US_LEFT_ECHO_PIN, MAX_DISTANCE},
    {US_LEFT_FRONT_TRIG_PIN, US_LEFT_FRONT_ECHO_PIN, MAX_DISTANCE},
    {US_FRONT_LEFT_TRIG_PIN, US_FRONT_LEFT_ECHO_PIN, MAX_DISTANCE}
};

//ir
#define NUM_IR 6
#define TIMEOUT 2500  


//QTRX sensor
QTRSensorsRC qtr((unsigned char[]) {QTRX_DT1, QTRX_DT2, QTRX_DT3, QTRX_DT4, QTRX_DT5, QTRX_DT6}, NUM_IR, TIMEOUT, QTRX_CTRL);


float irFilteredValues[NUM_IR+1];
unsigned int irRawValues[NUM_IR];
unsigned int irRawPosition;

int filter(int value, float* lastValue){
    float a = 0.3;

    // Serial.print(value);
    // Serial.print(",");

    float filteredValue = ((1-a)*(*lastValue)) + (a*value);
    // float filteredValue = value;
    *lastValue = filteredValue;

    // Serial.println(filteredValue);

    return filteredValue;

}


void readUS(){
    for(int i = 0; i < NUM_US; i++){
        // read and filter us sensors
        usFilteredValues[i] = filter(usSensors[i].ping_cm(), &usFilteredValues[i]);
        delay(10);
    }
}

void readIR(){
    
    // read raw ir values 
    irRawPosition = qtr.readLine(irRawValues);

     // filter raw ir values
    for (int i = 0; i < NUM_IR; i++)
    {  
        irFilteredValues[i] = filter(irRawValues[i], &irFilteredValues[i]);
    }
    irFilteredValues[NUM_IR] = filter(irRawPosition, &irFilteredValues[NUM_IR]);
}


void sendData(){
    for(int i = 0; i < NUM_US; i++){
        Serial1.print(usFilteredValues[i]);
        Serial1.print(";");
    }

    for (int j = 0; j < NUM_IR; j++){
        Serial1.print(irFilteredValues[j]);
        Serial1.print(";");
    }

    Serial1.println(irRawPosition);
}

void printData(){
    
    Serial.println("US:");

    Serial.print("FRONT RIGHT:  ");
    Serial.println(usFilteredValues[0]);

    Serial.print("RIGHT FRONT:  ");
    Serial.println(usFilteredValues[1]);

    Serial.print("RIGHT:  ");
    Serial.println(usFilteredValues[2]);

    Serial.print("BACK RIGHT:  ");
    Serial.println(usFilteredValues[3]);

    Serial.print("BACK LEFT:  ");
    Serial.println(usFilteredValues[4]);

    Serial.print("LEFT:  ");
    Serial.println(usFilteredValues[5]);

    Serial.print("LEFT FRONT:  ");
    Serial.println(usFilteredValues[6]);

    Serial.print("FRONT LEFT:  ");
    Serial.println(usFilteredValues[7]);

    for (int j = 0; j < NUM_IR; j++){
        Serial.print(j+1);
        Serial.print(": ");
        Serial.println(irRawValues[j]);
    }

    Serial.print("Position: ");

    Serial.println(irRawPosition);

    Serial.println("#######################");

}

void calibrateQTRX(){
    for (uint16_t i = 0; i < 400; i++)
    {
        qtr.calibrate();
    }

    // print the calibration minimum values measured when emitters were on
    for (uint8_t i = 0; i < NUM_IR; i++)
    {
        Serial.print(qtr.calibratedMinimumOn[i]);
        Serial.print(' ');
    }
    Serial.println();

    // print the calibration maximum values measured when emitters were on
    for (uint8_t i = 0; i < NUM_IR; i++)
    {
        Serial.print(qtr.calibratedMaximumOn[i]);
        Serial.print(' ');
    }
    Serial.println();
    delay(1000);
}


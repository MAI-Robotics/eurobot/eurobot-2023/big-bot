#include <QTRSensors.h>
#include "pinsSensors.h"
#include "sensors.h"

void setup() {
  Serial.begin(115200);
  Serial1.begin(115200);

  
  //us sensor setup
  pinMode(US_FRONT_LEFT_TRIG_PIN, OUTPUT);
  pinMode(US_FRONT_LEFT_ECHO_PIN, INPUT);

  pinMode(US_FRONT_RIGHT_TRIG_PIN, OUTPUT);
  pinMode(US_FRONT_RIGHT_ECHO_PIN, INPUT);

  pinMode(US_RIGHT_TRIG_PIN, OUTPUT);
  pinMode(US_RIGHT_ECHO_PIN, INPUT);

  pinMode(US_BACK_RIGHT_TRIG_PIN, OUTPUT);
  pinMode(US_BACK_RIGHT_ECHO_PIN, INPUT);
  
  pinMode(US_BACK_LEFT_TRIG_PIN, OUTPUT);
  pinMode(US_BACK_LEFT_ECHO_PIN, INPUT);

  pinMode(US_LEFT_TRIG_PIN, OUTPUT);
  pinMode(US_LEFT_ECHO_PIN, INPUT);

  pinMode(US_RIGHT_FRONT_TRIG_PIN, OUTPUT);
  pinMode(US_RIGHT_FRONT_ECHO_PIN, INPUT);

  pinMode(US_LEFT_FRONT_TRIG_PIN, OUTPUT);
  pinMode(US_LEFT_FRONT_ECHO_PIN, INPUT);
  

  pinMode(25,OUTPUT);
   
  calibrateQTRX();
  digitalWrite(25,HIGH);

}

void loop() {
  readUS();
  readIR();
  sendData();
  //printData();
}
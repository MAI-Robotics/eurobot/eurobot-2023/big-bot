//ultrasonic sensors

    #define US_FRONT_RIGHT_TRIG_PIN 4   
    #define US_FRONT_RIGHT_ECHO_PIN 5

    #define US_RIGHT_FRONT_TRIG_PIN 6
    #define US_RIGHT_FRONT_ECHO_PIN 7

    #define US_RIGHT_TRIG_PIN 3
    #define US_RIGHT_ECHO_PIN 21

    #define US_BACK_RIGHT_TRIG_PIN 22  
    #define US_BACK_RIGHT_ECHO_PIN 28

    #define US_BACK_LEFT_TRIG_PIN 17
    #define US_BACK_LEFT_ECHO_PIN 16

    #define US_LEFT_TRIG_PIN 2
    #define US_LEFT_ECHO_PIN 20

    #define US_LEFT_FRONT_TRIG_PIN 19
    #define US_LEFT_FRONT_ECHO_PIN 18

    #define US_FRONT_LEFT_TRIG_PIN 27  
    #define US_FRONT_LEFT_ECHO_PIN 26

//infrared sensor array
#define QTRX_DT1 8
#define QTRX_DT2 9
#define QTRX_DT3 10
#define QTRX_DT4 11
#define QTRX_DT5 12 
#define QTRX_DT6 13
#define QTRX_CTRL 14
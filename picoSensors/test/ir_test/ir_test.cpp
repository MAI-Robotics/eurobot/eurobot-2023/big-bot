#include <Arduino.h>
#include <QTRSensors.h>
#include <pinsSensors.h>


#define NUM_SENSORS   6     
#define TIMEOUT       2500  

unsigned int sensorValues[NUM_SENSORS];

QTRSensorsRC qtr((unsigned char[]) {QTRX_DT1, QTRX_DT2, QTRX_DT3, QTRX_DT4, QTRX_DT5, QTRX_DT6}, NUM_SENSORS, TIMEOUT, QTRX_CTRL);

void setup()
{
    Serial.begin(115200);

    // // configure the sensors
    // qtr.setTypeRC();
    // qtr.setSensorPins((const uint8_t[]){QTRX_DT1, QTRX_DT2, QTRX_DT3, QTRX_DT4, QTRX_DT5, QTRX_DT6}, SensorCount);

    // 2.5 ms RC read timeout (default) * 10 reads per calibrate() call
    // = ~25 ms per calibrate() call.
    // Call calibrate() 400 times to make calibration take about 10 seconds.
    for (uint16_t i = 0; i < 400; i++)
    {
        qtr.calibrate();
    }

    // print the calibration minimum values measured when emitters were on
    for (uint8_t i = 0; i < NUM_SENSORS; i++)
    {
        Serial.print(qtr.calibratedMinimumOn[i]);
        Serial.print(' ');
    }
    Serial.println();

    // print the calibration maximum values measured when emitters were on
    for (uint8_t i = 0; i < NUM_SENSORS; i++)
    {
        Serial.print(qtr.calibratedMaximumOn[i]);
        Serial.print(' ');
    }
    Serial.println();
    delay(1000);
}

void loop()
{
    // read calibrated sensor values and obtain a measure of the line position
    // from 0 to 5000 (for a white line, use readLineWhite() instead)
    uint16_t position = qtr.readLine(sensorValues);

    // qtr.read(sensorValues);

    // print the sensor values as numbers from 0 to 1000, where 0 means maximum
    // reflectance and 1000 means minimum reflectance, followed by the line
    // position
    for (uint8_t i = 0; i < NUM_SENSORS; i++)
    {
        Serial.print(sensorValues[i]);
        Serial.print('\t');
    }
    Serial.println(position);

    delay(250);
}
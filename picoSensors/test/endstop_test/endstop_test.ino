/*
//endstop pin
#define endstop_pin 2

//Motor pins
#define right_enable 4
#define left_enable 6
#define right_PWM 5
#define left_PWM 7

//motor encoder pins
#define ENCA 2 //yellow
#define ENCB 3 //brown
// green --> GND
//orange --> 5V

void setup() {
  Serial.begin(9600);
  
  //endstop setup
  pinMode(endstop_pin, INPUT_PULLUP); 

  //Motor setup
  pinMode(right_enable, OUTPUT);
  pinMode(left_enable, OUTPUT);
  pinMode(right_PWM, OUTPUT);
  pinMode(left_PWM, OUTPUT);
  digitalWrite(right_enable, HIGH);
  digitalWrite(left_enable, HIGH);  
}

void loop() {
  if(digitalRead(endstop_pin) == LOW)
  {
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); 
    delay(4000);
    analogWrite(left_PWM, 30);
    analogWrite(right_PWM, 0);   
    delay(6200);

  }
  digitalWrite(LED_BUILTIN, LOW);
  analogWrite(left_PWM, 0);
  analogWrite(right_PWM, 30);
}
*/


//endstop pin
#define endstop_pin 53

void setup() {
  // put your setup code here, to run once:
  //endstop setup
  pinMode(endstop_pin, INPUT_PULLUP); 
  pinMode(LED_BUILTIN, OUTPUT); 
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(endstop_pin) == LOW)
  {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(50);

  }
  digitalWrite(LED_BUILTIN, LOW);
}

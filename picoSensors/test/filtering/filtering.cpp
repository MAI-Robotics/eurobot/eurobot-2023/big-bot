#include <NewPing.h>

#define TRIG_PIN 3
#define ECHO_PIN 4
#define MAX_DISTANCE 400

float a = 0.3;
int value;
float lastValue;
float filterdValue;

NewPing usSensor(TRIG_PIN, ECHO_PIN, MAX_DISTANCE);

void setup()
{ 
    Serial.begin(115200); 

    pinMode(TRIG_PIN, OUTPUT);
    pinMode(ECHO_PIN, INPUT);

    lastValue = usSensor.ping_cm();

}

void filter(){
    filterdValue = (1-a)*lastValue + a*value;
    lastValue = filterdValue;

    Serial.print(value);
    Serial.print(" ");
    Serial.println(filterdValue);
}

void loop()
{
    value = usSensor.ping_cm(); //return current distance (cm) in serial
    filter();

    delay(60);                   // we suggest to use over 60ms measurement cycle, in order to prevent trigger signal to the echo signal.
}
#include <Arduino.h>
#include <pinsSensors.h>


void readUsSensor(int trigPin, int echoPin){
    // Clears the TRIG_PIN
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    // Sets the TRIG_PIN on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
    int duration = pulseIn(echoPin, HIGH);
    // Calculating the distance
    int distance = duration * 0.034 / 2;

    return distance;
}

void setup() {
    Serial.begin(115200); // Starts the serial communication

    //pinModes
    pinMode(US_FRONT_LEFT_TRIG_PIN, OUTPUT);
    pinMode(US_FRONT_LEFT_ECHO_PIN, INPUT);

    pinMode(US_FRONT_RIGHT_TRIG_PIN, OUTPUT);
    pinMode(US_FRONT_RIGHT_ECHO_PIN, INPUT);

    pinMode(US_RIGHT_TRIG_PIN, OUTPUT);
    pinMode(US_RIGHT_ECHO_PIN, INPUT);

    pinMode(US_BACK_RIGHT_TRIG_PIN, OUTPUT);
    pinMode(US_BACK_RIGHT_ECHO_PIN, INPUT);

    pinMode(US_BACK_LEFT_TRIG_PIN, OUTPUT);
    pinMode(US_BACK_LEFT_ECHO_PIN, INPUT);

    pinMode(US_LEFT_TRIG_PIN, OUTPUT);
    pinMode(US_LEFT_ECHO_PIN, INPUT);

    Serial.println("Setup for US sensor test complete");
  
}
void loop() {

    Serial.print("FRONT LEFT: ");
    Serial.println(readUsSensor(US_FRONT_LEFT_TRIG_PIN, US_FRONT_LEFT_ECHO_PIN));

    delay(20);

    Serial.print("FRONT RIGHT: ");
    Serial.println(readUsSensor(US_FRONT_RIGHT_TRIG_PIN, US_FRONT_RIGHT_ECHO_PIN));
    
    delay(20);

    Serial.print("RIGHT: ");
    Serial.println(readUsSensor(US_RIGHT_TRIG_PIN, US_RIGHT_ECHO_PIN));

    delay(20);

    Serial.print("BACK RIGHT: ");
    Serial.println(readUsSensor(US_BACK_RIGHT_TRIG_PIN, US_BACK_RIGHT_ECHO_PIN));

    delay(20);

    Serial.print("BACK LEFT: ");
    Serial.println(readUsSensor(US_BACK_LEFT_TRIG_PIN, US_BACK_LEFT_ECHO_PIN));

    delay(20);

    Serial.print("LEFT: ");
    Serial.println(readUsSensor(US_LEFT_TRIG_PIN, US_LEFT_ECHO_PIN));

    delay(20);

    Serial.println("############################################");

}
#include <Arduino.h>
#include "pinsSensors.h"
#include <NewPing.h>
#include <QTRSensors.h>

#define MAX_DISTANCE 300

// us sensors
int usSensorCount = 6;

NewPing usSensorFrontLeft(US_FRONT_LEFT_TRIG_PIN, US_FRONT_LEFT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorFrontRight(US_FRONT_RIGHT_TRIG_PIN, US_FRONT_RIGHT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorRight(US_RIGHT_TRIG_PIN, US_RIGHT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorBackRight(US_BACK_RIGHT_TRIG_PIN, US_BACK_RIGHT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorBackLeft(US_BACK_LEFT_TRIG_PIN, US_BACK_LEFT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorLeft(US_LEFT_TRIG_PIN, US_LEFT_ECHO_PIN, MAX_DISTANCE);

float usFilteredValues[6];

//ir sensors
QTRSensors qtr;

const uint8_t irSensorCount = 6;
float irFilteredValues[irSensorCount+1];
uint16_t irRawValues[irSensorCount];
uint16_t irRawPosition;

int filter(int value, float* lastValue){
    float a = 0.3;

    Serial.print(value);
    Serial.print(",");

    float filteredValue = ((1-a)*(*lastValue)) + (a*value);
    *lastValue = filteredValue;

    Serial.println(filteredValue);

    return filteredValue;

}

void readUS(){

    // read and filter us sensors
    usFilteredValues[0] = filter(usSensorFrontLeft.ping_cm(), &usFilteredValues[0]);
    delay(20);
    usFilteredValues[1] = filter(usSensorFrontRight.ping_cm(), &usFilteredValues[1]);
    delay(20);
    usFilteredValues[2] = filter(usSensorRight.ping_cm(), &usFilteredValues[2]);
    delay(20);
    usFilteredValues[3] = filter(usSensorBackRight.ping_cm(), &usFilteredValues[3]);
    delay(20);
    usFilteredValues[4] = filter(usSensorBackLeft.ping_cm(), &usFilteredValues[4]);
    delay(20);
    usFilteredValues[5] = filter(usSensorLeft.ping_cm(), &usFilteredValues[5]);
  
}

void readIR(){
    
    // read raw ir values 
    irRawPosition = qtr.readLineBlack(irRawValues);

    // filter raw ir values
    for (int i = 0; i < usSensorCount; i++)
    {  
        irFilteredValues[i] = filter(irRawValues[i], &irFilteredValues[i]);
    }
    irFilteredValues[irSensorCount] = filter(irRawPosition, &irFilteredValues[6]);
}

void sendData(){
    for(int i = 0; i < usSensorCount; i++){
        Serial1.print(usFilteredValues[i]);
        Serial1.print(";");
    }

    for (int j = 0; j < irSensorCount; j++){
        Serial1.print(irFilteredValues[j]);
        Serial1.print(";");
    }

    Serial1.println(irFilteredValues[irSensorCount]);
}

void printData(){
    
    Serial.println("US");

    for(int i = 0; i < usSensorCount; i++){
        Serial.print(i+1);
        Serial.print(": ");
        Serial.println(usFilteredValues[i]);

    }

    Serial.println("IR");

    for (int j = 0; j < irSensorCount; j++){
        Serial.print(j+1);
        Serial.print(": ");
        Serial.println(irFilteredValues[j]);
    }

    Serial.print("Position: ");

    Serial.println(irFilteredValues[irSensorCount]);

}

void setup()
{ 
    Serial.begin(115200);
    Serial1.begin(115200);

    // configure the ir sensors
    qtr.setTypeRC();
    qtr.setSensorPins((const uint8_t[]){IR_1_PIN, IR_2_PIN, IR_3_PIN, IR_4_PIN, IR_5_PIN, IR_6_PIN}, irSensorCount);
    qtr.setEmitterPin(IR_CTRL_PIN);

    //pinModes
    pinMode(US_FRONT_LEFT_TRIG_PIN, OUTPUT);
    pinMode(US_FRONT_LEFT_ECHO_PIN, INPUT);

    pinMode(US_FRONT_RIGHT_TRIG_PIN, OUTPUT);
    pinMode(US_FRONT_RIGHT_ECHO_PIN, INPUT);

    pinMode(US_RIGHT_TRIG_PIN, OUTPUT);
    pinMode(US_RIGHT_ECHO_PIN, INPUT);

    pinMode(US_BACK_RIGHT_TRIG_PIN, OUTPUT);
    pinMode(US_BACK_RIGHT_ECHO_PIN, INPUT);

    pinMode(US_BACK_LEFT_TRIG_PIN, OUTPUT);
    pinMode(US_BACK_LEFT_ECHO_PIN, INPUT);

    pinMode(US_LEFT_TRIG_PIN, OUTPUT);
    pinMode(US_LEFT_ECHO_PIN, INPUT);


    // read us for one time 
    usFilteredValues[0] = usSensorFrontLeft.ping_cm();
    usFilteredValues[1] = usSensorFrontRight.ping_cm();
    usFilteredValues[2] = usSensorRight.ping_cm();
    usFilteredValues[3] = usSensorBackRight.ping_cm();
    usFilteredValues[4] = usSensorBackLeft.ping_cm();
    usFilteredValues[5] = usSensorLeft.ping_cm();

    delay(100);

    // read ir for one time 
    irRawPosition = qtr.readLineBlack(irRawValues);
    for (uint8_t i = 0; i < usSensorCount; i++)
    {  
        irFilteredValues[i] = irRawValues[i];
    }
    irFilteredValues[irSensorCount] = irRawPosition;
}

void loop(){
    readIR();
    readUS();

    sendData();
    printData();
}

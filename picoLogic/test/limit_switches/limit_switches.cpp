#include <Arduino.h>
#include <pinsLogic.h>

void setup(){
    Serial.begin(115200);

    pinMode(LOWER_ENDSTOP_PIN, INPUT_PULLUP);
    pinMode(UPPER_ENDSTOP_PIN, INPUT_PULLUP);
    pinMode(PULL_CORD_PIN, INPUT_PULLUP);
}

void loop(){
    Serial.print("PULL_CORD_PIN:  ");
    Serial.println(digitalRead(PULL_CORD_PIN));

    Serial.print("UPPER_ENDSTOP_PIN:  ");
    Serial.println(digitalRead(UPPER_ENDSTOP_PIN));

    Serial.print("LOWER_ENDSTOP_PIN:  ");
    Serial.println(digitalRead(LOWER_ENDSTOP_PIN));


    Serial.println("####################################");

    delay(500);

}
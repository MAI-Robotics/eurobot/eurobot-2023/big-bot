//endstop pin
#define end_stop_pin 2

//pull cord pin
#define pull_cord_pin 12

//motor pins
#define right_enable 4
#define left_enable 6
#define right_PWM 5
#define left_PWM 7

//motor encoder
#define ENCA 2 //yellow
#define ENCB 3 //brown
// green --> GND
//orange --> 5V

//stepper motor pins
#define ENABLE_PIN 11
#define STEP_PIN A1
#define DIR_PIN_LEFT 12
#define DIR_PIN_RIGHT 3

//servo pins
#define SERVO_1 8
#define SERVO_2 9
#define SERVO_3 10
//servo pins
#define SERVO_1 8
#define SERVO_2 9
#define SERVO_3 10

//endstop pin
#define end_stop_pin 2

//pull cord pin
#define pull_cord_pin 12

//motor pins
#define right_enable 4
#define left_enable 6
#define right_PWM 5
#define left_PWM 7

//motor encoder
#define ENCA 1 //yellow
#define ENCB 3 //brown
// green --> GND
//orange --> 5V

//servo stuff
#include <Servo.h>
Servo myservo1;
Servo myservo2;
Servo myservo3;

int servo_open = 50;
int servo_closed = 0;

volatile int pos = 0;
long target = -1300;

void loadCherries(){
    myservo1.write(servo_closed);
    myservo2.write(servo_open);
    myservo3.write(servo_open);

    delay(2500);

    myservo1.write(servo_closed);
    myservo2.write(servo_closed);
    myservo3.write(servo_open);

    delay(2500);

    myservo1.write(servo_closed);
    myservo2.write(servo_closed);
    myservo3.write(servo_closed);
    
    delay(2500);
}

void emptyCherries(){
    myservo1.write(servo_open);
    myservo2.write(servo_open);
    myservo3.write(servo_open);
    
    delay(2500);
}

void dispenseCherry(){
    myservo1.write(servo_open); // dispenses one cherry
    delay(500);
    myservo1.write(servo_closed);
    delay(500);
    myservo2.write(servo_open); // drops on down to position 1
    delay(500);
    myservo2.write(servo_closed);
    delay(500);
    myservo3.write(servo_open); // drops on down to position 2
    delay(500);
    myservo3.write(servo_closed);
}

void home(){
    while(!(digitalRead(end_stop_pin) == LOW)){
        analogWrite(left_PWM, 0);
        analogWrite(right_PWM, 30);
    }
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); 
    delay(3500);
    digitalWrite(LED_BUILTIN, HIGH);
    pos = 0;
}

void setup(){
    //Serial.begin(9600);

    //endstop setup
    pinMode(end_stop_pin, INPUT_PULLUP); 
    pinMode(LED_BUILTIN, OUTPUT); 

    //Motor setup
    pinMode(right_enable, OUTPUT);
    pinMode(left_enable, OUTPUT);
    pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);
    digitalWrite(right_enable, HIGH);
    digitalWrite(left_enable, HIGH);

    //encoder setup
    pinMode(ENCA,INPUT);
    pinMode(ENCB,INPUT);

    myservo1.attach(SERVO_1);
    myservo2.attach(SERVO_2);
    myservo3.attach(SERVO_3);


} 

void loop(){
  if(digitalRead(end_stop_pin) == LOW){
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
  }
  digitalWrite(LED_BUILTIN, LOW);
  delay(30);
}
#include <Arduino.h>
#include <AccelStepper.h>
#include <pinsLogic.h>


AccelStepper leftStepper(1, STEP_PIN_LEFT, DIR_PIN_LEFT);
AccelStepper rightStepper(1, STEP_PIN_RIGHT, DIR_PIN_RIGHT);

int stepsPerFullTurn = 3034;

void drive(int steps, bool dir){
    if(dir){
        leftStepper.move(-steps);
        rightStepper.move(steps);
    } else {
        leftStepper.move(steps);
        rightStepper.move(-steps);
    }

    while(abs(rightStepper.distanceToGo()) > 0 && abs(leftStepper.distanceToGo()) > 0){
        leftStepper.run();
        rightStepper.run();
    }
}


void turnRight(){
    leftStepper.move(stepsPerFullTurn/2);
    rightStepper.move(stepsPerFullTurn/2);

    while(abs(rightStepper.distanceToGo()) > 0 && abs(leftStepper.distanceToGo()) > 0){
        leftStepper.run();
        rightStepper.run();
    }
}


void turnLeft(){
    leftStepper.move(-(stepsPerFullTurn/2));
    rightStepper.move(-(stepsPerFullTurn/2));

    while(abs(rightStepper.distanceToGo()) > 0 && abs(leftStepper.distanceToGo()) > 0){
        leftStepper.run();
        rightStepper.run();
    }
}


void turn180(){
    leftStepper.move(stepsPerFullTurn);
    rightStepper.move(stepsPerFullTurn);

    while(abs(rightStepper.distanceToGo()) > 0 && abs(leftStepper.distanceToGo()) > 0){
        leftStepper.run();
        rightStepper.run();
    }
}

void setup() {

    Serial.begin(115200);
    Serial.println("Starting acceleration test");

    pinMode(STEPPER_ENABLE_PIN_LEFT, OUTPUT);
    pinMode(STEPPER_ENABLE_PIN_RIGHT, OUTPUT);
    pinMode(STEP_PIN_LEFT, OUTPUT);
    pinMode(DIR_PIN_LEFT, OUTPUT);
    pinMode(STEP_PIN_RIGHT, OUTPUT);
    pinMode(DIR_PIN_RIGHT, OUTPUT);

    leftStepper.setMaxSpeed(2000);
    leftStepper.setAcceleration(4000);

    rightStepper.setMaxSpeed(2000);
    rightStepper.setAcceleration(4000);


    delay(3000);
}

void loop(){
    turnRight();
    delay(1000);

    turnLeft();
    delay(1000);

    turn180();
    delay(1000);

    turn180();
    delay(1000);
}
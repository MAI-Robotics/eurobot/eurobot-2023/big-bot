#include "pinsLogic.h"

void driveForward(int steps){
  digitalWrite(DIR_PIN_RIGHT,HIGH); 
  digitalWrite(DIR_PIN_LEFT,LOW); 
  for(int x = 0; x < steps; x++) {
    digitalWrite(STEP_PIN_RIGHT,HIGH); 
    digitalWrite(STEP_PIN_LEFT, HIGH);
    delayMicroseconds(250); 
    digitalWrite(STEP_PIN_RIGHT,LOW); 
    digitalWrite(STEP_PIN_LEFT, LOW);
    delayMicroseconds(250); 
  }
}

// void driveBackward(int steps){
//   digitalWrite(DIR_PIN_RIGHT,LOW); 
//   digitalWrite(DIR_PIN_LEFT,HIGH); 
//   for(int x = 0; x < steps; x++) {
//     digitalWrite(STEP_PIN,HIGH); 
//     delayMicroseconds(1000); 
//     digitalWrite(STEP_PIN,LOW); 
//     delayMicroseconds(1000); 
//   }
// }

// void turn90degrees(char direction){
//   if(direction == 'l'){
//     digitalWrite(DIR_PIN_RIGHT,LOW); 
//     digitalWrite(DIR_PIN_LEFT,LOW); 
//     for(int x = 0; x < 1520; x++) {
//       digitalWrite(STEP_PIN,HIGH); 
//       delayMicroseconds(1000); 
//       digitalWrite(STEP_PIN,LOW); 
//       delayMicroseconds(1000); 
//       }  
//   }
//   if(direction == 'r'){
//     digitalWrite(DIR_PIN_RIGHT,HIGH); 
//     digitalWrite(DIR_PIN_LEFT,HIGH); 
//     for(int x = 0; x < 1520; x++) {
//       digitalWrite(STEP_PIN,HIGH); 
//       delayMicroseconds(1000); 
//       digitalWrite(STEP_PIN,LOW); 
//       delayMicroseconds(1000); 
//       }  
//   }
// }

void setup(){
    pinMode(STEP_PIN_LEFT, OUTPUT);
    pinMode(STEP_PIN_RIGHT, OUTPUT);
    pinMode(STEPPER_ENABLE_PIN_LEFT, OUTPUT);
    pinMode(STEPPER_ENABLE_PIN_RIGHT, OUTPUT);
    pinMode(DIR_PIN_RIGHT, OUTPUT);
    pinMode(DIR_PIN_LEFT, OUTPUT);

    digitalWrite(STEPPER_ENABLE_PIN_LEFT, LOW);
    digitalWrite(STEPPER_ENABLE_PIN_RIGHT, LOW);

    driveForward(5000);
}

void loop() {
  
}
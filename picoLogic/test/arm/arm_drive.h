// Servo lowerServo;
// Servo centerServo;
// Servo upperServo;

int servo_open = 50;
int servo_closed = 0;

/*
void loadCherries(){
    myservo1.write(servo_closed);
    myservo2.write(servo_open);
    myservo3.write(servo_open);

    delay(2500);

    myservo1.write(servo_closed);
    myservo2.write(servo_closed);
    myservo3.write(servo_open);

    delay(2500);

    myservo1.write(servo_closed);
    myservo2.write(servo_closed);
    myservo3.write(servo_closed);
    
    delay(2500);
}

void emptyCherries(){
    myservo1.write(servo_open);
    myservo2.write(servo_open);
    myservo3.write(servo_open);
    
    delay(2500);
}

void dispenseCherry(){
    myservo1.write(servo_open); // dispenses one cherry
    delay(500);
    myservo1.write(servo_closed);
    delay(500);
    myservo2.write(servo_open); // drops on down to position 1
    delay(500);
    myservo2.write(servo_closed);
    delay(500);
    myservo3.write(servo_open); // drops on down to position 2
    delay(500);
    myservo3.write(servo_closed);
}

*/

void moveUp(){
    analogWrite(DC_LEFT_PWM_PIN, 45);
    analogWrite(DC_RIGHT_PWM_PIN, 0);
    while((digitalRead(UPPER_ENDSTOP_PIN)  == HIGH)){
        delay(1);
    }
    analogWrite(DC_LEFT_PWM_PIN, 0);
    analogWrite(DC_RIGHT_PWM_PIN, 0); 
    delay(1000);
}

void moveDown(){
    analogWrite(DC_LEFT_PWM_PIN, 0);
    analogWrite(DC_RIGHT_PWM_PIN, 45);
    while(digitalRead(LOWER_ENDSTOP_PIN)  == HIGH){
        delay(5);
    }
    analogWrite(DC_LEFT_PWM_PIN, 0);
    analogWrite(DC_RIGHT_PWM_PIN, 0); 
    delay(1000);
}
#include <Arduino.h>
#include <pinsLogic.h>
#include <arm_drive.h>

void setup(){
    Serial.begin(115200);

    pinMode(LOWER_ENDSTOP_PIN, INPUT_PULLUP);
    pinMode(UPPER_ENDSTOP_PIN, INPUT_PULLUP);

    pinMode(DC_ENABLE_PIN, OUTPUT);
    pinMode(DC_RIGHT_PWM_PIN, OUTPUT);
    pinMode(DC_LEFT_PWM_PIN, OUTPUT);
    
    digitalWrite(DC_ENABLE_PIN, HIGH);
}

void loop(){
    Serial.print("PULL_CORD_PIN:  ");
    Serial.println(digitalRead(PULL_CORD_PIN));

    Serial.print("UPPER_ENDSTOP_PIN:  ");
    Serial.println(digitalRead(UPPER_ENDSTOP_PIN));

    Serial.print("LOWER_ENDSTOP_PIN:  ");
    Serial.println(digitalRead(LOWER_ENDSTOP_PIN));


    Serial.println("####################################");


    moveUp();

    delay(1000);

    moveDown();

    delay(1000);

}
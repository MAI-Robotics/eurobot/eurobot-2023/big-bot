#include <Arduino.h>
#include <AccelStepper.h>
#include <pinsLogic.h>


AccelStepper leftStepper(1, STEP_PIN_LEFT, DIR_PIN_LEFT);
AccelStepper rightStepper(1, STEP_PIN_RIGHT, DIR_PIN_RIGHT);
bool direction = true;



void setup() {

  Serial.begin(115200);
  Serial.println("Starting acceleration test");

  pinMode(STEPPER_ENABLE_PIN_LEFT, OUTPUT);
  pinMode(STEPPER_ENABLE_PIN_RIGHT, OUTPUT);
  pinMode(STEP_PIN_LEFT, OUTPUT);
  pinMode(DIR_PIN_LEFT, OUTPUT);
  pinMode(STEP_PIN_RIGHT, OUTPUT);
  pinMode(DIR_PIN_RIGHT, OUTPUT);

  leftStepper.setMaxSpeed(2000);
  leftStepper.setAcceleration(4000);

  rightStepper.setMaxSpeed(2000);
  rightStepper.setAcceleration(4000);
  

  leftStepper.move(-10000);
  rightStepper.move(10000);

}

void loop() {
  if(abs(rightStepper.distanceToGo()) <= 0 && abs(leftStepper.distanceToGo()) <= 0){
    if(direction == true){
      rightStepper.move(-10000);
      leftStepper.move(10000);

      direction = false;

    } else {
      rightStepper.move(10000);
      leftStepper.move(-10000);

      direction = true;
    }
  }
  
  leftStepper.run();
  rightStepper.run();

}

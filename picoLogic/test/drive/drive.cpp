#include <Arduino.h>
#include <stepper_drive.h>

void setup(){
    //stepper
    pinMode(STEPPER_ENABLE_PIN_LEFT, OUTPUT);
    pinMode(STEPPER_ENABLE_PIN_RIGHT, OUTPUT);
    pinMode(STEP_PIN_LEFT, OUTPUT);
    pinMode(DIR_PIN_LEFT, OUTPUT);
    pinMode(STEP_PIN_RIGHT, OUTPUT);
    pinMode(DIR_PIN_RIGHT, OUTPUT);
}

void loop(){
    driveFast(5000, true, false);
    delay(250);
    driveFast(5000, false, false);
}
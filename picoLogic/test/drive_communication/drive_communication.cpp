#include <Arduino.h>
// #include <AccelStepper.h>

#include "pinsLogic.h"
#include "drive.h"

const int STRING_SIZE = 70;

const int US_SENSORS = 6;
const int IR_SENSORS = 7;
const int arraySize = US_SENSORS + IR_SENSORS;


char inputString[STRING_SIZE]; //Initialized variable to store recieved data
int outputArray[arraySize];

int usValues[US_SENSORS];
int irValues[IR_SENSORS];

void splitString(){ 
    int i = 0;  
    char *p = strtok(inputString, ";");

    while(p != NULL) {
        Serial.print("Section found: ");
        Serial.println(p);

        outputArray[i++] = atoi(p);
        p = strtok(NULL, ";");
    }
}

void getSensorValues(){
    
    splitString();

    Serial.print("US Sensor Values: ");
    for(int i = 0; i < US_SENSORS; i++){
        usValues[i] = outputArray[i];
        Serial.print(usValues[i]);
        Serial.print(", ");
    }
    Serial.println("");

    Serial.print("IR Sensor Values: ");
    for(int i = 0; i < IR_SENSORS; i++){
        irValues[i] = outputArray[i+US_SENSORS];
        Serial.print(irValues[i]);
        Serial.print(", ");
    }
    Serial.println("");
}

void setup(){
    Serial.begin(115200); //connect to PC
    Serial1.begin(115200); //connect to other Pico
    Serial.println("Started drive & us test script");

    pinMode(STEPPER_ENABLE_PIN_LEFT, OUTPUT);
    pinMode(STEPPER_ENABLE_PIN_RIGHT, OUTPUT);
    pinMode(STEP_PIN_LEFT, OUTPUT);
    pinMode(DIR_PIN_LEFT, OUTPUT);
    pinMode(STEP_PIN_RIGHT, OUTPUT);
    pinMode(DIR_PIN_RIGHT, OUTPUT);

    digitalWrite(STEPPER_ENABLE_PIN_LEFT, LOW);
    digitalWrite(STEPPER_ENABLE_PIN_RIGHT, LOW);

    pinMode(25, OUTPUT);
    digitalWrite(25, LOW);

}

void loop() {
    while(Serial1.available() <= 0){
        digitalWrite(25, HIGH);
    }
    digitalWrite(25, LOW);
    
    Serial1.readStringUntil('\n').toCharArray(inputString, STRING_SIZE);
    Serial.println(inputString);
  	

    getSensorValues();

    // if(usValues[0] <= 20 && usValues[1] <= 20 && usValues[3] <= 20 && usValues[4] <= 20){
    //     stop_driving = false;
    // } else {
    //     stop_driving = false;
    // }
  
}

void loop1(){
//   driveFast(2000, true);
//   driveFast(2000, false);
}

#include <math.h>
#include <stdio.h>
#include <AccelStepper.h>
#include <MultiStepper.h>

AccelStepper leftStepper(1, STEP_PIN_LEFT, DIR_PIN_LEFT);
AccelStepper rightStepper(1, STEP_PIN_RIGHT, DIR_PIN_RIGHT);

/* data */
/**** Some constants ****/
const float wheelDiameter = 70;         // mm
const float outerWheelDistance = 219.91; // mm
const float wheelWidth = 20.00;         // mm
const float stepsPerRev = 2057;     // Steps that are needed to do one revolution
float distancePer360Rotation = 589.5; //mm
const float stepsPerFullTurn = 6068;

// Calculate the values for later calculating the amount of steps
const float midWheelDistance = outerWheelDistance - wheelWidth;
const float distancePerStep = PI * wheelDiameter / stepsPerRev;               // mm
const float anglePerStep = 360 / ((PI * outerWheelDistance) / distancePerStep); //°

float x_pos = 0.0;      //current x pos
float y_pos = 0.0;      //current y pos
float direction = 0.0;  //current angle

int startMillis = 0;

void setStartMillis(){
    startMillis = millis();
}

bool timeLeft(){
    if(millis()-startMillis >= 98500){
        analogWrite(DC_LEFT_PWM_PIN, 0);
        analogWrite(DC_RIGHT_PWM_PIN, 0);
        while(true){
            delay(1000);
            digitalWrite(LED_PIN, HIGH);
        }
        return false;
    } else {
        return true;
    }
}

bool obstacleFront(){
    if((usValues[0] < 30 && usValues[0] > 0) || (usValues[1] < 30 && usValues[1] > 0) || (usValues[6] < 30 && usValues[6] > 0) || (usValues[7] < 30 && usValues[7] > 0)){
        return true;
    } else {
        return false;
    }
}

bool obstacleBack(){
    if((usValues[3] < 30 && usValues[3] > 0) || (usValues[4] < 30 && usValues[4] > 0) || (usValues[2] < 30 && usValues[2] > 0) || (usValues[5] < 30 && usValues[5] > 0)){
        return true;
    } else {
        return false;
    }
}

bool obstacleAnywhere(){
    if((usValues[3] < 30 && usValues[3] > 0) || (usValues[4] < 30 && usValues[4] > 0) || (usValues[2] < 30 && usValues[2] > 0) || (usValues[5] < 30 && usValues[5] > 0) || (usValues[0] < 30 && usValues[0] > 0) || (usValues[1] < 30 && usValues[1] > 0) || (usValues[6] < 30 && usValues[6] > 0) || (usValues[7] < 30 && usValues[7] > 0)){
        return true;
    } else {
        return false;
    }
}

// bool obstacleFront(){
//     int count = int(usValues[0] < 55 && usValues[0] > 0) + (usValues[1] < 55 && usValues[1] > 0) + (usValues[6] < 55 && usValues[6] > 0) + (usValues[7] < 55 && usValues[7] > 0);
//     if(count >= 2){
//         return true;
//     } else {
//         return false;
//     }
    
// }

// bool obstacleBack(){
//     int count = int(usValues[3] < 55 && usValues[3] > 0) + int(usValues[4] < 45 && usValues[4] > 0) + int(usValues[2] < 55 && usValues[6] > 0) + int(usValues[5] < 45 && usValues[7] > 0);
//     if(count >= 2){
//         return true;
//     } else {
//         return false;
//     }
    
// }

// bool obstacleAnywhere(){
//     int count = int(usValues[3] < 55 && usValues[3] > 0) + int(usValues[4] < 45 && usValues[4] > 0) + int(usValues[2] < 55 && usValues[6] > 0) + int(usValues[5] < 45 && usValues[7] > 0) + int(usValues[3] < 55 && usValues[3] > 0) + int(usValues[4] < 55 && usValues[4] > 0) + int(usValues[2] < 55 && usValues[6] > 0) + int(usValues[5] < 55 && usValues[7] > 0);
//     if(count >= 2){
//         return true;
//     } else {
//         return false;
//     }
    
// }


/**
 * @description: drives either forward or backward a given amount of steps
 * @param steps: amount of steps to drive
 * @param dir: true(clockwise)/false(counterclockwise)
 * @param CoA: collision avoidance
*/
void drive(int steps, bool dir, bool CoA){
    

    if(dir){
        rightStepper.move(steps);
        leftStepper.move(-steps);
    } else {
        rightStepper.move(-steps);
        leftStepper.move(steps);
    }

    while((abs(rightStepper.distanceToGo()) > 0 || abs(leftStepper.distanceToGo()) > 0) && timeLeft()){
        leftStepper.run();
        rightStepper.run();

        // x_pos = (dir ? x_pos + distancePerStep * cos(direction*PI/180) : x_pos - distancePerStep * cos(direction*PI/180));
        // y_pos = (dir ? y_pos + distancePerStep * sin(direction*PI/180) : y_pos + distancePerStep * sin(direction*PI/180));
        
        if(CoA){
            if(dir){
                if(obstacleFront()){
                    int rightDistanceToGo = rightStepper.distanceToGo();
                    int leftDistanceToGo = leftStepper.distanceToGo();

                    rightStepper.setAcceleration(340000);
                    leftStepper.setAcceleration(340000);

                    rightStepper.stop();
                    leftStepper.stop();

                    int timestamp = millis();

                    while(obstacleFront() || timestamp > millis() - 500){
                        leftStepper.run();
                        rightStepper.run(); 
                        timeLeft();
                    }

                    delay(600);

                    rightStepper.setAcceleration(1700);
                    leftStepper.setAcceleration(1700);

                    rightStepper.move(rightDistanceToGo);
                    leftStepper.move(leftDistanceToGo);
                }
            } else{
                if(obstacleBack()){
                    int rightDistanceToGo = rightStepper.distanceToGo();
                    int leftDistanceToGo = leftStepper.distanceToGo();

                    rightStepper.setAcceleration(340000);
                    leftStepper.setAcceleration(340000);

                    rightStepper.stop();
                    leftStepper.stop();

                    int timestamp = millis();

                    while(obstacleBack() || timestamp > millis() - 500){
                        leftStepper.run();
                        rightStepper.run(); 
                        timeLeft();
                    }

                    delay(600);

                    rightStepper.setAcceleration(1700);
                    leftStepper.setAcceleration(1700);

                    rightStepper.move(rightDistanceToGo);
                    leftStepper.move(leftDistanceToGo);
                
                }
            }
        }      
    }
}

/**
 * @description: drives until it detects a line
 * @param steps: steps to drive
 * @param dir: true(forward)/false(backward)
 * @param CoA: collision avoidance 
*/
void driveUntilLine(unsigned int steps, bool CoA, unsigned int offset){
    while(abs(rightStepper.distanceToGo()) > 0 || abs(leftStepper.distanceToGo()) > 0){
        for(int i = 0; i < 6; i++){
            if(irValues[i] < 900){
                leftStepper.stop();
                rightStepper.stop();

                while(rightStepper.distanceToGo() > 0 || leftStepper.distanceToGo() > 0){
                    leftStepper.run();
                    rightStepper.run();
                    timeLeft();

                }

                break;
                
            }
        }

        leftStepper.run();
        rightStepper.run();

        
        
    }
    
}

/**
 * @description: drives a set amount of mm
 * @param distance: distance in mm to drive
 * @param dir: true(forward)/false(backward)
 * @param condition: specifies a limit
 * @param CoA: collision avoidance 
*/
void driveDistanceMM(unsigned int distance, unsigned int distanceOffset, bool dir, char condition, bool CoA){         
    int steps = distance / outerWheelDistance * stepsPerRev;
    switch(condition){
      case 'l':
        driveUntilLine(steps, CoA, distanceOffset);
        break;
      case 'n':
        drive(steps, dir, CoA);
        break;
    }
}

/**
 * @description: turns either right or left a certain angle
 * @param degree: angle to turn
 * @param dir: true(clockwise)/false(counterclockwise)
 * @param CoA: collision avoidance 
*/
void turnAngle(float degree, bool dir, bool CoA){
      
    int steps = round(degree/360 * stepsPerFullTurn);
    if(dir){
        rightStepper.move(-steps);
        leftStepper.move(-steps);
    } else {
        rightStepper.move(steps);
        leftStepper.move(steps);
    }



    while((abs(rightStepper.distanceToGo()) > 0 || abs(leftStepper.distanceToGo()) > 0) && timeLeft()){

        leftStepper.run();
        rightStepper.run();


        if(obstacleAnywhere() && CoA){
            int rightDistanceToGo = rightStepper.distanceToGo();
            int leftDistanceToGo = leftStepper.distanceToGo();

            int passedStepsRight = steps - abs(rightDistanceToGo); 
            int passedStepsLeft = steps - abs(leftDistanceToGo);

            rightStepper.setAcceleration(340000);
            leftStepper.setAcceleration(340000);

            rightStepper.stop();
            leftStepper.stop();

            int timestamp = millis();

            while(obstacleAnywhere() || timestamp > millis() - 500){
                rightStepper.run();
                leftStepper.run(); 
                timeLeft();  
            }

            delay(2000);

            rightStepper.setAcceleration(1700);
            leftStepper.setAcceleration(1700);

            if(dir){
                // if(rightStepper.speed() > 1000 && leftStepper.speed() > 1000){
                //     rightStepper.move(rightDistanceToGo+60);
                //     leftStepper.move(leftDistanceToGo+60);
                // } else if(rightStepper.speed() > 1600 && leftStepper.speed() > 1600){
                //     rightStepper.move(rightDistanceToGo+140);
                //     leftStepper.move(leftDistanceToGo+140);
                // } else {
                //     rightStepper.move(rightDistanceToGo+215);
                //     leftStepper.move(leftDistanceToGo+215);
                // }

                rightStepper.move(rightDistanceToGo);
                leftStepper.move(leftDistanceToGo);
            } else {
                // if(rightStepper.speed() > 1000 && leftStepper.speed() > 1000){
                //     rightStepper.move(rightDistanceToGo-60);
                //     leftStepper.move(leftDistanceToGo-60);
                // } else if(rightStepper.speed() > 1600 && leftStepper.speed() > 1600){
                //     rightStepper.move(rightDistanceToGo-140);
                //     leftStepper.move(leftDistanceToGo-140);
                // } else {
                //     rightStepper.move(rightDistanceToGo-215);
                //     leftStepper.move(leftDistanceToGo-215);
                // }

                rightStepper.move(rightDistanceToGo);
                leftStepper.move(leftDistanceToGo);
            }
        }
    }
}

/**
 * @description: turns left and right over the line to calibrate the QTRX sensor
*/
void calibrationRoutine(){
    turnAngle(20, true, false);
    delay(500);

    while(millis() < 18000){
        turnAngle(55, false, false);
        delay(500);
        turnAngle(55,true, false);
        delay(500);
    }
    turnAngle(20, false, false);
}

void driveTo(float angle, float distance, bool CoA) {
  float turn = angle-direction; //angle to turn  
  turnAngle((turn>=0.0?turn:-turn), (turn>=0.0?false:true), CoA);
  direction = angle;
  distance += 0.5;    //round
  driveDistanceMM((unsigned int)distance, 0, true, 'n', CoA);
}
 
 
void driveToCoordinate(int x, int y, bool CoA) {
  float deltaX = x - x_pos;
  float deltaY = y - y_pos;
  float distance = sqrt((deltaX*deltaX) + (deltaY*deltaY));
  float angle = 0.0;
 
  if (deltaX == 0) {
    if (deltaY > 0) {
      angle = 90.0;
    } else if (deltaY < 0) {
      angle = 270.0;
    }
  } else {
    angle = atan2(deltaY,deltaX) * 180/PI;
  }

  driveTo(angle, distance, CoA);
  return;
}

#include <Servo.h>

// Servo lowerServo;
Servo centerServo;
Servo upperServo;

int servo_open = 80;
int servo_closed = 0;

void loadCherries(){
    // lowerServo.write(servo_closed);
    centerServo.write(servo_open);
    upperServo.write(servo_open);

    delay(2500);

    // lowerServo.write(servo_closed);
    centerServo.write(servo_closed);
    upperServo.write(servo_open);

    delay(2500);

    // lowerServo.write(servo_closed);
    centerServo.write(servo_closed);
    upperServo.write(servo_closed);
    
    delay(2500);
}

void emptyCherries(){
    // lowerServo.write(servo_open);
    centerServo.write(servo_open);
    upperServo.write(servo_open);
}


void dispenseCherry(){
    // lowerServo.write(servo_open); // dispenses one cherry
    delay(500);
    // lowerServo.write(servo_closed);
    delay(500);
    timeLeft();
    centerServo.write(servo_open); // drops one down to position 1
    delay(500);
    timeLeft();
    centerServo.write(servo_closed);
    delay(500);
    timeLeft();
    upperServo.write(servo_open); // drops one down to position 2
    delay(500);
    timeLeft();
    upperServo.write(servo_closed);
}


void startLED(){
    digitalWrite(LED_PIN, HIGH);
}

void stopLED(){
    digitalWrite(LED_PIN, LOW);
}

void moveUp(){
    analogWrite(DC_LEFT_PWM_PIN, 0);
    analogWrite(DC_RIGHT_PWM_PIN, 90);
    while(digitalRead(UPPER_ENDSTOP_PIN)  == HIGH && timeLeft()){
        delay(1);
    }
    analogWrite(DC_LEFT_PWM_PIN, 0);
    analogWrite(DC_RIGHT_PWM_PIN, 0); 
}


bool gripCakeLayer(){
    bool cakeExists = false;
    
    // startSuction();

    analogWrite(DC_LEFT_PWM_PIN, 90);
    analogWrite(DC_RIGHT_PWM_PIN, 0);

    while(digitalRead(CAKE_ENDSTOP_PIN) == HIGH && digitalRead(LOWER_ENDSTOP_PIN) == HIGH){
        delay(1);
    }

    if(digitalRead(CAKE_ENDSTOP_PIN) == LOW){
        delay(20);
        analogWrite(DC_LEFT_PWM_PIN, 0);
        analogWrite(DC_RIGHT_PWM_PIN, 0); 
        cakeExists = true;
    } else if(digitalRead(LOWER_ENDSTOP_PIN) == LOW){
        analogWrite(DC_LEFT_PWM_PIN, 0);
        analogWrite(DC_RIGHT_PWM_PIN, 0); 
        cakeExists = false;
    }

    if(cakeExists){
        moveUp();
        dispenseCherry();
        return true;
    } else {
        moveUp();
        return false;
    }
}

void placeCherry(){    

    analogWrite(DC_LEFT_PWM_PIN, 90);
    analogWrite(DC_RIGHT_PWM_PIN, 0);

    while((digitalRead(CAKE_ENDSTOP_PIN) == HIGH && digitalRead(LOWER_ENDSTOP_PIN) == HIGH) && timeLeft()){
        delay(1);
    }

    analogWrite(DC_LEFT_PWM_PIN, 0);
    analogWrite(DC_RIGHT_PWM_PIN, 0);

    dispenseCherry();
    moveUp();
}

void dropCakeLayer(bool lastLayer){
    analogWrite(DC_LEFT_PWM_PIN, 90);
    analogWrite(DC_RIGHT_PWM_PIN, 0);
    if(!lastLayer){
        delay(250);
    }
    delay(1000);
    analogWrite(DC_LEFT_PWM_PIN, 0);
    analogWrite(DC_RIGHT_PWM_PIN, 0); 
    delay(250);
    moveUp();
}
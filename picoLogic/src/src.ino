#include <Arduino.h>
#include <stdlib.h>
#include "pinsLogic.h"
#include "communication.h"
#include "stepper_drive.h"
#include "arm_drive.h"
#include "logic.h"

void setup(){
    Serial.begin(115200);
    Serial1.begin(115200);

    //stepper
    pinMode(STEPPER_ENABLE_PIN_LEFT, OUTPUT);
    pinMode(STEPPER_ENABLE_PIN_RIGHT, OUTPUT);
    pinMode(STEP_PIN_LEFT, OUTPUT);
    pinMode(DIR_PIN_LEFT, OUTPUT);
    pinMode(STEP_PIN_RIGHT, OUTPUT);
    pinMode(DIR_PIN_RIGHT, OUTPUT);

    leftStepper.setMaxSpeed(2000);
    leftStepper.setAcceleration(1700);

    rightStepper.setMaxSpeed(2000);
    rightStepper.setAcceleration(1700);


    //limit switches
    pinMode(LOWER_ENDSTOP_PIN, INPUT_PULLUP); 
    pinMode(UPPER_ENDSTOP_PIN, INPUT_PULLUP);
    pinMode(CAKE_ENDSTOP_PIN, INPUT_PULLUP);
    pinMode(PULL_CORD_PIN, INPUT_PULLUP);

    //teamselect
    pinMode(TEAMSELECT_PIN, INPUT_PULLUP);

    //valve
    pinMode(LED_PIN, OUTPUT);

    //pico led
    pinMode(25, OUTPUT); 

    //Motor setup
    pinMode(DC_ENABLE_PIN, OUTPUT);
    pinMode(DC_RIGHT_PWM_PIN, OUTPUT);
    pinMode(DC_LEFT_PWM_PIN, OUTPUT);
    digitalWrite(DC_ENABLE_PIN, HIGH);  

    //servo setup
    // lowerServo.attach(LOWER_SERVO);
    centerServo.attach(CENTER_SERVO);
    upperServo.attach(UPPER_SERVO);


    //tactics 
    startUpRoutine();

    tacticV1(getTeam(), true);

    // startLED();
    
}
 

void setup1(){
} 

void loop(){
}

void loop1(){
    if(Serial1.available() > 0){
        Serial1.readStringUntil('\n').toCharArray(inputString, STRING_SIZE);
        Serial.println(inputString);

        getSensorValues();
    }
    
}

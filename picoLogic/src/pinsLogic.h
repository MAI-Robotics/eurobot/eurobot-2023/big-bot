//pull cord pin
#define PULL_CORD_PIN 18

//dc motor pins
#define DC_RIGHT_PWM_PIN 11
#define DC_LEFT_PWM_PIN 10
#define DC_ENABLE_PIN 12

//left stepper motor pins
#define STEPPER_ENABLE_PIN_LEFT 2
#define DIR_PIN_LEFT 3
#define STEP_PIN_LEFT 4

//right stepper motor pins
#define STEPPER_ENABLE_PIN_RIGHT 6
#define DIR_PIN_RIGHT 7
#define STEP_PIN_RIGHT 8

//servo pins
#define LOWER_SERVO 15
#define CENTER_SERVO 17
#define UPPER_SERVO 16

//endstops
#define CAKE_ENDSTOP_PIN 9
#define UPPER_ENDSTOP_PIN 20
#define LOWER_ENDSTOP_PIN 5

//teamselect
#define TEAMSELECT_PIN 19

//led
#define LED_PIN 14
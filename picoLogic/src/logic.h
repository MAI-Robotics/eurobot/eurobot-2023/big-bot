void startUpRoutine(){
    digitalWrite(LED_PIN, LOW);
    stopLED();
    moveUp();
    emptyCherries();
    delay(500);
    loadCherries();
    delay(500);

    //calibrationRoutine();

    digitalWrite(25,HIGH);
    while(digitalRead(PULL_CORD_PIN)  == LOW){
        delay(5);
    }
    setStartMillis();
    digitalWrite(25,LOW);
    /*
    if(digitalRead(TEAMSELECT_PIN) = LOW){
        teamGreen();
    } else {
        teamBlue();
    }
    */

}

bool getTeam(){
    if(digitalRead(TEAMSELECT_PIN) == HIGH){
        return false;
    } else {
        return true;
    }
}

void getCakeLayer(int layer, int row){
    switch(layer){
        case 1:
            driveDistanceMM(1125-200, 0, true, 'n', false);
            gripCakeLayer();
            driveDistanceMM(1125-200, 0, false, 'n', false);
            turnAngle(90, false, false);
            dropCakeLayer(false);
            driveDistanceMM(100, 0 , true, 'n', false);
            driveDistanceMM(100, 0 , false, 'n', false);
            turnAngle(90, true, false);
            break;
        case 2:
            driveDistanceMM(775-200, 0, true, 'n', false);
            turnAngle(90, false, false);
            driveDistanceMM(500-100, 0, true, 'n', false);
            gripCakeLayer();
            driveDistanceMM(500-100, 0, false, 'n', false);
            turnAngle(90, true, false);
            driveDistanceMM(775-200, 0, false, 'n', false);
            turnAngle(90, false, false);
            driveDistanceMM(100, 0 , true, 'n', false);
            dropCakeLayer(false);
            driveDistanceMM(100, 0 , false, 'n', false);
            turnAngle(90, true, false);
            break;
        case 3:
            driveDistanceMM(575-200, 0, true, 'n', false);
            turnAngle(90, false, false);
            driveDistanceMM(500-100, 0, true, 'n', false);
            gripCakeLayer();
            driveDistanceMM(500-100, 0, false, 'n', false);
            turnAngle(90, true, false);
            driveDistanceMM(575-200, 0, false, 'n', false);
            turnAngle(90, false, false);
            driveDistanceMM(100, 0 , true, 'n', false);
            dropCakeLayer(true);
            dispenseCherry();
            driveDistanceMM(100, 0 , false, 'n', false);
            turnAngle(90, true, false);
            break;
    }
}

/**
 * @description: drives tactic version 1
 * @param team: true = green, false = blue
 * @param CoA: Colission Avoidance on or off
*/
void tacticV1(bool team, bool CoA){

    //1st cherry
    driveDistanceMM(420, 0, true, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(420, 0, true, 'n', CoA);
    placeCherry();

    //2nd cherry
    driveDistanceMM(300, 0, false, 'n', CoA);
    turnAngle(90, team?false:true, CoA);
    driveDistanceMM(200, 0, true, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(320, 0, true, 'n', CoA);
    placeCherry();

    //drive back
    driveDistanceMM(400, 0, false, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(500, 0, true, 'n', CoA);
    turnAngle(90, team?false:true, CoA);

    //drive onto plate
    driveDistanceMM(400, 0, true, 'n', CoA);
    turnAngle(90, team?false:true, false);
    driveDistanceMM(650, 0, true, 'n', CoA);


    //get 3rd cake
    driveDistanceMM(300, 0, false, 'n', CoA);
    turnAngle(90, team?false:true, CoA);
    driveDistanceMM(850, 0, true, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(500, 0, true, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(700, 0, true, 'n', CoA);
    placeCherry();

    //drive to endzone
    // driveDistanceMM(300, 0, false, 'n', CoA);
    // turnAngle(90, team?true:false, CoA);
    // driveDistanceMM(1100, 0, true, 'n', CoA);

    driveDistanceMM(350, 0, false, 'n', CoA);
    turnAngle(team?80:100, team?false:true, CoA);
    driveDistanceMM(1200, 0, true, 'n', CoA);
    turnAngle(55, team?true:false, false);
    driveDistanceMM(team?550:510, 0, true, 'n', false);




    //led on
    startLED();
}

/**
 * @description: drives tactic version 2
 * @param team: true = green, false = blue
 * @param CoA: Colission Avoidance on or off
*/
void tacticV2(bool team, bool CoA){
    //1st cherry
    driveDistanceMM(430, 0, true, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(410, 0, true, 'n', CoA);
    placeCherry();

    //2nd cherry
    driveDistanceMM(300, 0, false, 'n', CoA);
    turnAngle(90, team?false:true, CoA);
    driveDistanceMM(190, 0, true, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(310, 0, true, 'n', CoA);
    placeCherry();

    //get 3rd cake
    driveDistanceMM(900, 0, false, 'n', CoA);
    turnAngle(90, team?false:true, CoA);
    driveDistanceMM(330, 0, true, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(630, 0, true, 'n', CoA);
    placeCherry();

    //drive 1st and 2nd onto plate
    driveDistanceMM(180, 0, false, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(800, 0, true, 'n', CoA);
    turnAngle(90, team?false:true, CoA);
    driveDistanceMM(400, 0, true, 'n', CoA);
    turnAngle(90, team?false:true, CoA);
    driveDistanceMM(650, 0, true, 'n', CoA);

    //drive to end position
    driveDistanceMM(700, 0, false, 'n', CoA);
    turnAngle(90, team?true:false, CoA);
    driveDistanceMM(600, 0, false, 'n', CoA);

    //led on 
    startLED();
}

/**
 * @description: drives tactic version 3
 * @param team: true = green, false = blue
 * @param CoA: Colission Avoidance on or off
*/
void tacticV3(bool team, bool CoA){
    //get 3rd cake
    driveDistanceMM(100, 0, true, 'n', CoA);
    turnAngle(20, team?false:true, CoA);
    driveDistanceMM(900, 0, true, 'n', CoA);
    turnAngle(110, team?true:false, CoA);
    driveDistanceMM(650, 0, true, 'n', CoA);
    placeCherry();

    //drive back
    driveDistanceMM(350, 0, false, 'n', CoA);
    turnAngle(90, team?false:true, CoA);
    driveDistanceMM(1000, 0, false, 'n', CoA);



    //led on 
    startLED();
}


void checkEndstops(){
    while(true){
       if(digitalRead(UPPER_ENDSTOP_PIN)  == LOW ){
        digitalWrite(25,HIGH);
        delay(10);
    } else{
        digitalWrite(25,LOW);
        delay(10);
        } 
    }
}